def quicksort(listToSort, lowIndex, highIndex):
    if (highIndex - lowIndex > 0):
        p = partition(listToSort, lowIndex, highIndex)
        quicksort(listToSort,lowIndex,p-1)
        quicksort(listToSort, p+1,highIndex)

def partition(listToSort, lowIndex, highIndex):
    divider = lowIndex
    pivit = highIndex

    for i in range(lowIndex, highIndex):
        if (listToSort[i] < listToSort[pivot]):
            listToSort[i], listToSort[divider] = listToSort[divider], listToSort[i]
            divider += 1

    listToSort[pivot], listToSort[divider] = listToSort[divider], listToSort[pivot]

    return divider

testList1 = [1, 0, 2, 3, 4, 6, 5, 8, 9, 7]
testList2 = [3, 1, 5, 4, 6, 0, 9, 7, 2, 8]
testList3 = [3, 0, 6, 4, 5, 1, 9, 8, 2, 7]

quicksort(testList1,0,9)
print testList1
quicksort(testList2,0,9)
print testList2
quicksort(testList3,0,9)
print testList3